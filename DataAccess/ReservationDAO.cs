﻿using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class ReservationDAO : DAO
    {
        public void Insert(int idReservation)
        {
            DbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = CNX_STR;
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"update reservation
                                set datereservation = sysdate()
                                where idreservation = @idreservation;";
            cmd.Parameters.Add(new MySqlParameter("idreservation", idReservation));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'ajout d'une date de reservation. Details : "
                                        + ex.Message);
            }
        }
        public void Insert(int idDon, int idAdh, DateTime dateAjoutPanier)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection

            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO reservation 
                                (iddon, idadh, dateajoutpanier) 
                                VALUES 
                                (@iddon, @idadh, @dateajoutpanier);";

            cmd.Parameters.Add(new MySqlParameter("iddon", idDon));
            cmd.Parameters.Add(new MySqlParameter("idadh", idAdh));
            cmd.Parameters.Add(new MySqlParameter("dateajoutpanier", dateAjoutPanier));

            try
            {
                // 3 - ouvrir la connection :
                cnx.Open();
                // 4 - Exécuter la commande :
                cmd.ExecuteNonQuery();
                // 6 - fermer la conneciton :
                cnx.Close();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'ajout du don au panier. Details : "
                                        + exc.Message);
            }

        }
        public List<ReservationDetails> GetAllProductsInBasketByIdAdh(int id)
        {
            List<ReservationDetails> list = new List<ReservationDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"select r.*, p.nomProduit, d.quantite, u.nom 'unite', d.datelimite, d.numrue, d.libelevoie,
                                       v.codepostal, v.nom 'ville'
                                from reservation r
                                join don d on r.iddon = d.iddon
                                join produit p on d.idproduit = p.idproduit
                                join unite u on d.idunite = u.idunite
                                join ville v on d.idville = v.idville
                                where r.idadh = @idadh
                                and d.datelimite > sysdate()
                                and r.datereservation is null
                                and r.daterecuperation is null
                                and r.dateannulation is null
                                and r.dateajoutpanier is not null;";

            cmd.Parameters.Add(new MySqlParameter("idadh", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ReservationDetails res = new ReservationDetails();
                    res.IdReservation = dr.GetInt32(dr.GetOrdinal("idreservation"));
                    res.IdDon = dr.GetInt32(dr.GetOrdinal("iddon"));
                    res.IdAdh = dr.GetInt32(dr.GetOrdinal("idadh"));
                    if (!dr.IsDBNull(dr.GetOrdinal("dateajoutpanier")))
                    {
                        res.DateAjoutPanier = dr.GetDateTime(dr.GetOrdinal("dateajoutpanier"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("datereservation")))
                    {
                        res.DateReservation = dr.GetDateTime(dr.GetOrdinal("datereservation"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("daterecuperation")))
                    {
                        res.DateRecuperation = dr.GetDateTime(dr.GetOrdinal("daterecuperation"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("dateannulation")))
                    {
                        res.DateAnnulation = dr.GetDateTime(dr.GetOrdinal("dateannulation"));
                    }
                    res.NameProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
                    res.NbQuantity = dr.GetInt32(dr.GetOrdinal("quantite"));
                    res.NameUnity = dr.GetString(dr.GetOrdinal("unite"));
                    if (!dr.IsDBNull(dr.GetOrdinal("datelimite")))
                    {
                        res.ExpirationDate = dr.GetDateTime(dr.GetOrdinal("datelimite"));
                    }
                    res.NbStreet = dr.GetInt32(dr.GetOrdinal("numrue"));
                    res.NameStreet = dr.GetString(dr.GetOrdinal("libelevoie"));
                    res.PostalCode = dr.GetInt32(dr.GetOrdinal("codepostal"));
                    res.NameCity = dr.GetString(dr.GetOrdinal("ville"));
                    list.Add(res);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de tout le panier par idadh : " + ex.Message);
            }
            return list;
        }
        public List<ReservationDetails> GetAllBaskets()
        {
            List<ReservationDetails> list = new List<ReservationDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"select a.nom, count(r.idreservation) 'nbarticle', r.idadh
                                from reservation r
                                join don d on r.iddon = d.iddon
                                join adherent a on r.idadh = a.idadh
                                where d.datelimite > sysdate()
                                and r.datereservation is null
                                and r.daterecuperation is null
                                and r.dateannulation is null
                                and r.dateajoutpanier is not null
                                group by a.nom;";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ReservationDetails res = new ReservationDetails();
                    res.NameAdherent = dr.GetString(dr.GetOrdinal("nom"));
                    res.NbArticles = dr.GetInt32(dr.GetOrdinal("nbarticle"));
                    res.IdAdh = dr.GetInt32(dr.GetOrdinal("idadh"));
                    list.Add(res);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation du nombre d'article dans le panier de l'adherent : " + ex.Message);
            }
            return list;
        }
        public void Delete(int idDon, int idAdh)
        {
            DbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = CNX_STR;
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE
                                FROM reservation r
                                WHERE iddon = @iddon
                                and idadh != @idadh;";

            cmd.Parameters.Add(new MySqlParameter("iddon", idDon));
            cmd.Parameters.Add(new MySqlParameter("idadh", idAdh));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la suppression des reservations. Details : "
                                        + ex.Message);
            }
        }
        public void DeleteByIdAdh(int idDon, int idAdh)
        {
            DbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = CNX_STR;
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE
                                FROM reservation r
                                WHERE iddon = @iddon
                                and idadh = @idadh;";

            cmd.Parameters.Add(new MySqlParameter("iddon", idDon));
            cmd.Parameters.Add(new MySqlParameter("idadh", idAdh));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la suppression du don iddon deja présent dans le panier de l'adherent idadh. Details : "
                                        + ex.Message);
            }
        }
        public List<ReservationDetails> GetAll(int idAdh)
        {
            List<ReservationDetails> list = new List<ReservationDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"select r.iddon, p.nomProduit, d.quantite, u.nom 'nomunite', r.datereservation, d.datelimite, a.nom 'nomdonneur',
		                               a.prenom, e.numemplacement, d.numrue, d.libelevoie, d.complement, v.codepostal, v.nom 'ville', pa.nomsociete,
                                       r.idreservation
                                from reservation r
                                join don d on r.iddon = d.iddon
                                join produit p on d.idproduit = p.idproduit
                                join unite u on d.idunite = u.idunite
                                join adherent a on a.idadh = d.idadh
                                join ville v on d.idville = v.idville
                                left outer join emplacementstockage e on d.idemplacementstockage = e.idemplacementstockage
                                left outer join infrastockage i on e.idinfrastockage = i.idinfrastockage
                                left outer join partenaire pa on i.idpartenaire = pa.idpartenaire
                                where r.idadh = @idadh
                                and r.datereservation is not null
                                and r.daterecuperation is null
                                and r.dateannulation is null
                                and d.datelimite > sysdate()
                                ;";

            cmd.Parameters.Add(new MySqlParameter("idadh", idAdh));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ReservationDetails res = new ReservationDetails();

                    res.IdDon = dr.GetInt32(dr.GetOrdinal("iddon"));
                    res.IdReservation = dr.GetInt32(dr.GetOrdinal("idreservation"));
                    res.NameProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
                    res.NbQuantity = dr.GetInt32(dr.GetOrdinal("quantite"));
                    res.NameUnity = dr.GetString(dr.GetOrdinal("nomunite"));
                    res.DateReservation = dr.GetDateTime(dr.GetOrdinal("datereservation"));
                    res.ExpirationDate = dr.GetDateTime(dr.GetOrdinal("datelimite"));
                    if (!dr.IsDBNull(dr.GetOrdinal("nomdonneur")))
                    {
                        res.NameAdherent = dr.GetString(dr.GetOrdinal("nomdonneur"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("prenom")))
                    {
                        res.FirstNameAdherent = dr.GetString(dr.GetOrdinal("prenom"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("numemplacement")))
                    {
                        res.NbEmplacement = dr.GetInt32(dr.GetOrdinal("numemplacement"));
                    }
                    res.NbStreet = dr.GetInt32(dr.GetOrdinal("numrue"));
                    res.NameStreet = dr.GetString(dr.GetOrdinal("libelevoie"));
                    if (!dr.IsDBNull(dr.GetOrdinal("complement")))
                    {
                        res.ComplementStreet = dr.GetChar(dr.GetOrdinal("complement"));
                    }
                    res.PostalCode = dr.GetInt32(dr.GetOrdinal("codepostal"));
                    res.NameCity = dr.GetString(dr.GetOrdinal("ville"));
                    if (!dr.IsDBNull(dr.GetOrdinal("nomsociete")))
                    {
                        res.NameSociety = dr.GetString(dr.GetOrdinal("nomsociete"));
                    }

                    list.Add(res);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation des reservations de l'adherent : " + ex.Message);
            }

            return list;
        }
        public List<ReservationDetails> GetAllWithDateReservation()
        {
            List<ReservationDetails> list = new List<ReservationDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"select a.nom, count(r.idreservation) 'nbarticle', r.idadh
                                from reservation r
                                join don d on r.iddon = d.iddon
                                join adherent a on r.idadh = a.idadh
                                where d.datelimite > sysdate()
                                and r.datereservation is not null
                                and r.daterecuperation is null
                                and r.dateannulation is null
                                and r.dateajoutpanier is not null
                                group by a.nom;";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ReservationDetails res = new ReservationDetails();
                    res.NameAdherent = dr.GetString(dr.GetOrdinal("nom"));
                    res.NbArticles = dr.GetInt32(dr.GetOrdinal("nbarticle"));
                    res.IdAdh = dr.GetInt32(dr.GetOrdinal("idadh"));
                    list.Add(res);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation du nombre d'article dans les reservations de tous l'adherents : " + ex.Message);
            }

            return list;
        }
        public void Update(int idReservation)
        {
            DbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = CNX_STR;
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"update reservation
                                set dateannulation = sysdate(), datereservation = null, dateajoutpanier = null
                                where idreservation = @idreservation;";

            cmd.Parameters.Add(new MySqlParameter("idreservation", idReservation));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'ajout d'une date d'annulation à la réservation. Details : "
                                        + ex.Message);
            }
        }
    }
}
