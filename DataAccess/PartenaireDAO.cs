﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;


namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class PartenaireDAO :DAO
    {
        public List<PartenaireDetails> ToutPartenaireParVille(int idville)
        {

            List <PartenaireDetails> resultat = new List<PartenaireDetails> ();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT p.*, v.nom nomVille, v.codepostal
                                FROM partenaire p
                                JOIN ville v ON p.idville = v.idville
                                WHERE (v.idville = @idville OR @idville = 0)";

            cmd.Parameters.Add(new MySqlParameter("idville", idville));
            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                PartenaireDetails p = DataReaderToEntityPartenaireDetails(dr);

                resultat.Add(p);
            }

            cnx.Close();

            return resultat;
        }

        private PartenaireDetails DataReaderToEntityPartenaireDetails(DbDataReader dr)
        {
            PartenaireDetails partenaire = new PartenaireDetails();

            partenaire.Idpartenaire = dr.GetInt32(dr.GetOrdinal("idpartenaire"));
            partenaire.Idville = dr.GetInt32(dr.GetOrdinal("idville"));
            partenaire.Nomsociete = dr.GetString(dr.GetOrdinal("nomsociete"));
            partenaire.Numrue = dr.GetInt32(dr.GetOrdinal("numrue"));
            partenaire.Libelevoie = dr.GetString(dr.GetOrdinal("libelevoie"));
            partenaire.Numtelephone = dr.GetInt32(dr.GetOrdinal("numtelephone"));
            partenaire.PostalCode = dr.GetInt32(dr.GetOrdinal("codepostal"));
            partenaire.Nomville = dr.GetString(dr.GetOrdinal("nomVille"));

            return partenaire;
        }

        private Partenaire DataReaderToEntityPartenaire(DbDataReader dr)
        {
            Partenaire partenaire = new Partenaire();

            partenaire.Idpartenaire= dr.GetInt32(dr.GetOrdinal("idpartenaire"));
            partenaire.Idville = dr.GetInt32(dr.GetOrdinal("idville"));
            partenaire.Nomsociete = dr.GetString(dr.GetOrdinal("nomsociete"));
            partenaire.Numrue = dr.GetInt32(dr.GetOrdinal("numrue"));
            partenaire.Libelevoie = dr.GetString(dr.GetOrdinal("libelevoie"));
            partenaire.Numtelephone = dr.GetInt32(dr.GetOrdinal("numtelephone"));
            
            return partenaire;
        }
        public List<PartenaireDetails> ToutPartenaireDetails()
        {

            List<PartenaireDetails> resultat = new List<PartenaireDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT p.*, v.nom nomVille, v.codepostal
                                FROM partenaire p
                                JOIN ville v ON p.idville = v.idville";

           
            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                PartenaireDetails p = DataReaderToEntityPartenaireDetails(dr);

                resultat.Add(p);
            }

            cnx.Close();

            return resultat;
        }
    }
}
