﻿using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class UniteDAO : DAO
    {
        public List<Unite> GetAll()
        {
            List<Unite> list = new List<Unite>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from unite;";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Unite unite = new Unite();
                    unite.IdUnite = dr.GetInt32(dr.GetOrdinal("idunite"));
                    unite.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    list.Add(unite);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Erreur lors de la récuparation de toutes les unités : " + ex.Message);
            }
            return list;
        }
        public List<Unite> GetAllByIdProduit(int id)
        {
            List<Unite> list = new List<Unite>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"select u.nom, u.idunite
                                from mesurer m inner join unite u
                                on u.idunite = m.idunite
                                and m.idproduit = @idproduit;";
            cmd.Parameters.Add(new MySqlParameter("idproduit", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Unite unite = new Unite();
                    unite.IdUnite = dr.GetInt32(dr.GetOrdinal("idunite"));
                    unite.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    list.Add(unite);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de la liste d'unite idproduit = " + id + " : " + ex.Message);
            }
            return list;
        }
    }
}
