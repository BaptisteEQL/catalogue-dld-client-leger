﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data.Common;
using Fr.EQL.AI110.DLD_GE.Entities;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class DonDAO : DAO
    {
        public void Insert(Don don)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection
            // :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO don 
                                (idadh, idunite, idproduit, idville, quantite, dateajout
                                , numrue, libelevoie, datelimite, idemplacementstockage,complement, datedepotstockage ) 
                                VALUES 
                                (@idadh, @idunite, @idproduit, @idville, @quantite, @dateajout
                                , @numrue, @libelevoie, @datelimite, @idemplacementstockage,@complement, @datedepotstockage );";

            cmd.Parameters.Add(new MySqlParameter("idadh", don.IdAdh));
            cmd.Parameters.Add(new MySqlParameter("idunite", don.IdUnite));
            cmd.Parameters.Add(new MySqlParameter("idproduit", don.IdProduit));
            cmd.Parameters.Add(new MySqlParameter("idville", don.IdVille));
            if (don.IdEmplacementStockage != 0)
            {
                cmd.Parameters.Add(new MySqlParameter("idemplacementstockage", don.IdEmplacementStockage));
            }
            else
            {
                cmd.Parameters.Add(new MySqlParameter("idemplacementstockage", null));
            }
           
            cmd.Parameters.Add(new MySqlParameter("quantite", don.Quantite));
            cmd.Parameters.Add(new MySqlParameter("dateajout", don.DateAjout));
            cmd.Parameters.Add(new MySqlParameter("datedepotstockage", don.DateDepotStockage));
            cmd.Parameters.Add(new MySqlParameter("numrue", don.NumRue));
            cmd.Parameters.Add(new MySqlParameter("libelevoie", don.LibeleVoie));
            cmd.Parameters.Add(new MySqlParameter("complement", don.Complement));
            cmd.Parameters.Add(new MySqlParameter("datelimite", don.DateLimite));

            try
            {
                // 3 - ouvrir la connection :
                cnx.Open();

                // 4 - Exécuter la commande :
                int nbLignes = cmd.ExecuteNonQuery();
                Console.WriteLine(nbLignes);
                // 6 - fermer la conneciton :
                cnx.Close();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'insertion d'un don. Details : "
                                        + exc.Message);
            }
        }
        public List<DonDetails> RechercheTotal()
        {
            List<DonDetails> result = new List<DonDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT d.*, p.nomProduit, v.nom, u.nom 'nameunite', a.nom 'nameadh', a.prenom 'firstnameadh'
                                FROM don d
                                JOIN produit p ON d.idproduit = p.idproduit
                                JOIN ville v ON d.idville = v.idville
                                left outer JOIN reservation r ON d.iddon = r.iddon
                                join unite u on u.idunite = d.idunite
                                join adherent a on d.idadh = a.idadh
                                where r.datereservation is null
                                and d.dateajout is not null
                                and d.datelimite > sysdate()
                                and d.datedepotstockage is not null
                                and d.dateannulation is null;";

            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                DonDetails don = new DonDetails();

                don.IdDon = dr.GetInt32(dr.GetOrdinal("iddon"));
                don.IdAdh = dr.GetInt32(dr.GetOrdinal("idadh"));
                don.Quantite = dr.GetInt32(dr.GetOrdinal("quantite"));
                don.DateLimite = dr.GetDateTime(dr.GetOrdinal("datelimite"));

                don.VilleNom = dr.GetString(dr.GetOrdinal("nom"));
                don.NomProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
                don.NomUnity = dr.GetString(dr.GetOrdinal("nameunite"));
                don.AdhName = dr.GetString(dr.GetOrdinal("nameadh"));
                don.AdhPrenom = dr.GetString(dr.GetOrdinal("firstnameadh"));

                result.Add(don);
            }

            cnx.Close();

            return result;
        }
        public List<DonDetails> RechercheMultiCri(string catNom, string sousCatNom, string nomProduit, string villeNom)
        {

            List<DonDetails> result = new List<DonDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT distinct d.*, c.nom nomcategorie, s.nom nomsouscategorie, p.nomProduit, v.nom nomville, u.nom 'nomunite', a.prenom
                                FROM don d
                                JOIN produit p ON d.idproduit = p.idproduit
                                JOIN souscategorie s ON p.idsouscategorie = s.idsouscategorie
                                JOIN categorie c ON c.idcategorie = s.idcategorie
                                JOIN ville v ON v.idville = d.idville
                                left outer JOIN reservation r ON d.iddon = r.iddon
                                join unite u on d.idunite = u.idunite
                                join adherent a on d.idadh = a.idadh
                                WHERE c.nom LIKE @catNom
                                AND s.nom LIKE @sousCatNom
                                AND p.nomProduit LIKE @nomProduit
                                AND v.nom LIKE @villeNom
                                and r.datereservation is null
                                and d.datelimite > sysdate()
                                and d.datedepotstockage is not null
                                and d.dateannulation is null;";

            catNom = "%" + catNom + "%";
            sousCatNom = "%" + sousCatNom + "%";
            nomProduit = "%" + nomProduit + "%";
            villeNom = "%" + villeNom + "%";
            cmd.Parameters.Add(new MySqlParameter("catNom", catNom));
            cmd.Parameters.Add(new MySqlParameter("sousCatNom", sousCatNom));
            cmd.Parameters.Add(new MySqlParameter("nomProduit", nomProduit));
            cmd.Parameters.Add(new MySqlParameter("villeNom", villeNom));
            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                DonDetails don = DataReaderToEntityDon(dr);

                result.Add(don);
            }

            cnx.Close();

            return result;
        }
        private DonDetails DataReaderToEntityDon(DbDataReader dr)
        {
            DonDetails don = new DonDetails();

            don.IdDon = dr.GetInt32(dr.GetOrdinal("iddon"));
            don.IdAdh = dr.GetInt32(dr.GetOrdinal("idadh"));
            don.IdUnite = dr.GetInt32(dr.GetOrdinal("idunite"));
            don.IdProduit = dr.GetInt32(dr.GetOrdinal("idproduit"));
            don.IdVille = dr.GetInt32(dr.GetOrdinal("idville"));
            don.NomUnity = dr.GetString(dr.GetOrdinal("nomunite"));
            //don.IdEmplacementStockage = dr.GetInt32(dr.GetOrdinal("id"));
            //don.Photo = dr.GetString(dr.GetOrdinal("id"));
            don.Quantite = dr.GetInt32(dr.GetOrdinal("quantite"));
            //don.NumRue = dr.GetInt32(dr.GetOrdinal("id"));
            //don.LibeleVoie = dr.GetString(dr.GetOrdinal("id"));
            //don.Complement = dr.GetChar(dr.GetOrdinal("id"));
            don.CatNom = dr.GetString(dr.GetOrdinal("nomcategorie"));
            don.SousCatNom = dr.GetString(dr.GetOrdinal("nomsouscategorie"));
            don.NomProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
            don.VilleNom = dr.GetString(dr.GetOrdinal("nomville"));
            don.AdhPrenom = dr.GetString(dr.GetOrdinal("prenom"));
            don.DateLimite = dr.GetDateTime(dr.GetOrdinal("datelimite"));



            return don;
        }
        // Permet récupération des infos sur tous les dons d'un adherent
        public List<DonDetails> GetAllByIdAdh(int id)
        {
            List<DonDetails> list = new List<DonDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"select p.nomProduit, d.dateajout, d.datelimite, d.dateannulation, r.datereservation, r.daterecuperation, u.nom,
		                            d.quantite, d.iddon, d.datedepotstockage, d.idemplacementstockage,
                                    d.numrue, d.libelevoie, v.nom 'nomville', v.codepostal
                            from don d left outer join produit p on d.idproduit = p.idproduit
		                               left outer join reservation r on d.iddon = r.iddon and r.datereservation is not null
		                               left outer join unite u on d.idunite = u.idunite
                                       left outer join ville v on d.idville = v.idville
                            where d.idadh = @idadh
                            and d.dateajout is not null;";

            cmd.Parameters.Add(new MySqlParameter("idadh", id));

            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                DonDetails don = new DonDetails();

                don.IdDon = dr.GetInt32(dr.GetOrdinal("iddon"));
                don.NumRue = dr.GetInt32(dr.GetOrdinal("numrue"));
                don.PostalCode = dr.GetInt32(dr.GetOrdinal("codepostal"));
                don.LibeleVoie = dr.GetString(dr.GetOrdinal("libelevoie"));
                don.VilleNom = dr.GetString(dr.GetOrdinal("nomville"));
                if (!dr.IsDBNull(dr.GetOrdinal("idemplacementstockage")))
                {
                    don.IdEmplacementStockage = dr.GetInt32(dr.GetOrdinal("idemplacementstockage"));
                } else
                {
                    don.IdEmplacementStockage=0;
                }
                don.Quantite = dr.GetInt32(dr.GetOrdinal("quantite"));
                don.NomUnity = dr.GetString(dr.GetOrdinal("nom"));
                //NomProduit -> Details
                don.NomProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
                if (!dr.IsDBNull(dr.GetOrdinal("datedepotstockage")))
                {
                    don.DateDepotStockage = dr.GetDateTime(dr.GetOrdinal("datedepotstockage"));
                }
                //DateAjout -> normal
                if (!dr.IsDBNull(dr.GetOrdinal("dateajout")))
                {
                    don.DateAjout = dr.GetDateTime(dr.GetOrdinal("dateajout"));
                }
                //DateLimite -> normal
                if (!dr.IsDBNull(dr.GetOrdinal("datelimite")))
                {
                    don.DateLimite = dr.GetDateTime(dr.GetOrdinal("datelimite"));
                }
                //DateAnnulation -> normal
                if (!dr.IsDBNull(dr.GetOrdinal("dateannulation")))
                {
                    don.DateAnnulation = dr.GetDateTime(dr.GetOrdinal("dateannulation"));
                }
                //DateReservation -> Details
                if (!dr.IsDBNull(dr.GetOrdinal("datereservation")))
                {
                    don.DateReservation = dr.GetDateTime(dr.GetOrdinal("datereservation"));
                }
                //DateRecuperation -> Details
                if (!dr.IsDBNull(dr.GetOrdinal("daterecuperation")))
                {
                    don.DateDelivery = dr.GetDateTime(dr.GetOrdinal("daterecuperation"));
                }

                list.Add(don);
            }

            cnx.Close();

            return list;
        }
        

        public void Cancel(int id, DateTime cancelDate)
        {
            
            //Déclarer et paramétrer connexion et commande :

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don
                                SET dateannulation = @cancelDate
                                WHERE iddon = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            cmd.Parameters.Add(new MySqlParameter("cancelDate", cancelDate));

            try
            {
                
                // Ouvrir la connection et éxécuter la commande :
                
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'annulation du don : " + exc.Message);
            }
            finally
            {
                // Fermer connection MySQL :

                cnx.Close();
            }

        }

        public DonDetails GetByID(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT
                                p.nomProduit, c.nom catNom, s.nom sousCatNom, a.prenom adhPrenom, v.nom villeNom,
                                d.datelimite, d.quantite, u.nom uniteNom, d.numrue, d.libelevoie, v.codepostal
                                FROM don d
                                JOIN adherent a ON d.idadh = a.idadh
                                JOIN produit p ON d.idproduit = p.idproduit
                                JOIN ville v ON d.idville = v.idville
                                JOIN unite u ON d.idunite = u.idunite
                                JOIN souscategorie s ON p.idsouscategorie = s.idsouscategorie
                                JOIN categorie c ON c.idcategorie = s.idcategorie
                                WHERE d.iddon = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            DonDetails don = new DonDetails();

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    don.NomProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
                    don.AdhPrenom = dr.GetString(dr.GetOrdinal("adhPrenom"));
                    don.SousCatNom = dr.GetString(dr.GetOrdinal("sousCatNom"));
                    don.CatNom = dr.GetString(dr.GetOrdinal("catNom"));
                    don.NumRue = dr.GetInt32(dr.GetOrdinal("numrue"));
                    don.PostalCode = dr.GetInt32(dr.GetOrdinal("codepostal"));
                    don.LibeleVoie = dr.GetString(dr.GetOrdinal("libelevoie"));
                    don.VilleNom = dr.GetString(dr.GetOrdinal("villeNom"));
                    // if  null (dr is dbnull)

                    if (!dr.IsDBNull(dr.GetOrdinal("datelimite")))
                    {
                        don.DateLimite = dr.GetDateTime(dr.GetOrdinal("datelimite"));
                    }
                    don.Quantite = dr.GetInt32(dr.GetOrdinal("quantite"));
                    don.UniteNom = dr.GetString(dr.GetOrdinal("uniteNom"));
                }

        }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la récupération du don dans la BDD : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return don;
        }
    }
}
