﻿using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class VilleDAO : DAO
    {
        public Ville GetByIdAdh()
        {
            return null;
        }

        public List<Ville> GetAll()
        {
            List<Ville> list = new List<Ville>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from ville;";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Ville ville = new Ville();
                    ville.Idville = dr.GetInt32(dr.GetOrdinal("idville"));
                    ville.Codepostal = dr.GetInt32(dr.GetOrdinal("codepostal"));
                    ville.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    
                    list.Add(ville);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
               Console.WriteLine("Erreur lors de la récuparation de toutes les produits : " + ex.Message);
            }
            return list;
        }
        public Ville RechercheVilleParId(int Idville)
        {
            Ville ville = new Ville();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM ville
                                WHERE (idville = @idville OR idville = 0)";

            cmd.Parameters.Add(new MySqlParameter("idville", Idville));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    
                    ville.Idville = dr.GetInt32(dr.GetOrdinal("idville"));
                    ville.Codepostal = dr.GetInt32(dr.GetOrdinal("codepostal"));
                    ville.Nom = dr.GetString(dr.GetOrdinal("nom"));

                    
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Erreur lors de la récuparation de toutes les produits : " + ex.Message);
            }
            return ville;
        }

    }
}