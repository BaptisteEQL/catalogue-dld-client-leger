﻿using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class AdherentDAO : DAO
    {
        public List<Adherent> GetAll()
        {
            List<Adherent> list = new List<Adherent>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"select a.idadh, a.nom, count(d.iddon) 'nbdon'
                                from adherent a
                                left outer join don d on a.idadh = d.idadh
                                group by a.nom;";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Adherent adh = new Adherent();
                    adh.Idadh = dr.GetInt32(dr.GetOrdinal("idadh"));
                    adh.NbDon = dr.GetInt32(dr.GetOrdinal("nbdon"));
                    adh.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    list.Add(adh);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de tous les adherents : " + ex.Message);
            }
            return list;
        }
        public Adherent GetById(int id)
        {
            Adherent adh = new Adherent();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from adherent where idadh = @idadh;";
            cmd.Parameters.Add(new MySqlParameter("idadh", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    adh.Idadh = dr.GetInt32(dr.GetOrdinal("idadh"));
                    adh.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    adh.Prenom = dr.GetString(dr.GetOrdinal("prenom"));
                    adh.Idville = dr.GetInt32(dr.GetOrdinal("idville"));
                    adh.Numrue = dr.GetInt32(dr.GetOrdinal("numrue"));
                    adh.Libelevoie = dr.GetString(dr.GetOrdinal("libelevoie"));
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de l'adherent idadh = " + id + " : " + ex.Message);
            }
            return adh;
        }
    }
}
