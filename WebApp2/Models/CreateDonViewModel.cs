﻿using Fr.EQL.AI110.DLD_GE.Entities;

namespace Fr.EQL.AI110.DLD_GE.WebApp2.Models
{
    public class CreateDonViewModel
    {
        public Don Don { get; set; }
        public Categorie SelectedCategorie { get; set; }
        public Adherent SelectedAdherent { get; set; }
        public Souscategorie SelectedSousCategorie { get; set; }
        public Produit SelectedProduit { get; set; }
        public Unite SelectedUnity { get; set; }
        public List<Adherent> Adherents { get; set; }
        public List<Categorie> Categories { get; set; }
        public List<Souscategorie> SousCategories { get; set; }
        public List<Produit> Produits { get; set; }
        public List<Unite> Unites { get; set; }
        public List<DonDetails> DonDetails { get; set; }
        public List<Ville> Villes { get; set; }
        public int Idville { get; set; }
        public int Idinfrastockage { get; set; }
        public int Idemplacementstockage { get; set; }
        public int Numrue { get; set; }
        public string Libelevoie { get; set; }
        public char Complement { get; set; }
        
        public List<PartenaireDetails> PartenairesDetails { get; set;}
        public List<InfrastockageDetails> InfrastockageDetails { get; set; }
        public List<EmplacementstockageDetails> empsDetails { get; set; }
        public InfrastockageDetails InfrastockageDetailsChoisi { get; set; }

       

    }
}
