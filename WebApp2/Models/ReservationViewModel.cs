﻿using Fr.EQL.AI110.DLD_GE.Entities;

namespace Fr.EQL.AI110.DLD_GE.WebApp2.Models
{
    public class ReservationViewModel
    {
        public List<ReservationDetails> DonsInBasket { get; set; }
        public Adherent SelectedAdherent { get; set; }
        public int IdSelectedDonToRemove { get; set; }
    }
}
