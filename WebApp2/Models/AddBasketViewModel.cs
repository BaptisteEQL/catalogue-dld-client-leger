﻿using Fr.EQL.AI110.DLD_GE.Entities;

namespace Fr.EQL.AI110.DLD_GE.WebApp2.Models
{
    public class AddBasketViewModel
    {
        public Don SelectedDon { get; set; }
        public int SelectedIdDon { get; set; }
        public Adherent ConnectedAdherent { get; set; }
        public List<Adherent> Adherents { get; set; }
    }
}
