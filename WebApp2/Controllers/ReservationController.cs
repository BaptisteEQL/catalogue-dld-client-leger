﻿using Fr.EQL.AI110.DLD_GE.Business;
using Fr.EQL.AI110.DLD_GE.Entities;
using Fr.EQL.AI110.DLD_GE.WebApp2.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.DLD_GE.WebApp2.Controllers
{
    public class ReservationController : Controller
    {
        public IActionResult Index()
        {
            ReservationBusiness bu = new ReservationBusiness();
            List<ReservationDetails> list = new List<ReservationDetails>();
            list = bu.GetBasketsWithNbArticles();

            return View(list);
        }
        [HttpGet]
        public IActionResult AddBasket()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CatchDon(int idDon)
        {
            AddBasketViewModel model = new AddBasketViewModel();
            model.SelectedIdDon = idDon;

            AdherentBusiness bu = new AdherentBusiness();
            model.Adherents = bu.GetAdherents();

            return View("ChooseAdherent", model);
        }
        [HttpPost]
        public IActionResult AddBasket(AddBasketViewModel model)
        {
            ReservationBusiness reservationBusiness = new ReservationBusiness();

            Reservation reservation = new Reservation();
            reservation.DateAjoutPanier = DateTime.Now;

            reservationBusiness.SaveBasket(model.SelectedIdDon, model.ConnectedAdherent.Idadh, reservation.DateAjoutPanier);
            return RedirectToAction("DisplayBasket", new { id = model.ConnectedAdherent.Idadh });
        }
        public IActionResult DisplayBasket(int id)
        {
            ReservationBusiness bu = new ReservationBusiness();
            AdherentBusiness buAdh = new AdherentBusiness();

            ReservationViewModel model = new ReservationViewModel();
            model.DonsInBasket = bu.GetBasketByIdAdh(id);
            model.SelectedAdherent = buAdh.GetAdherentbyId(id);

            return View(model);
        }
        [HttpPost]
        public IActionResult MakeReservation(ReservationViewModel model)
        {
            ReservationBusiness bu = new ReservationBusiness();
            foreach (ReservationDetails r in bu.GetBasketByIdAdh(model.SelectedAdherent.Idadh))
            {
                bu.EditDateReservation(r.IdReservation, r.IdDon, r.IdAdh);
            }

            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult RemoveFromBasket(ReservationViewModel model)
        {
            ReservationBusiness bu = new ReservationBusiness();
            bu.DeleteDonInBasket(model.IdSelectedDonToRemove, model.SelectedAdherent.Idadh);

            return RedirectToAction("DisplayBasket", new { id = model.SelectedAdherent.Idadh });
        }
        public IActionResult DisplayReservation(int idAdh)
        {
            ReservationViewModel model = new ReservationViewModel();
            ReservationBusiness bu = new ReservationBusiness();
            AdherentBusiness adhBu = new AdherentBusiness();

            model.DonsInBasket = bu.GetAllReservationByIdAdh(idAdh);
            model.SelectedAdherent = adhBu.GetAdherentbyId(idAdh);

            return View(model);
        }
        public IActionResult DisplayAllAdherentsWithReservations()
        {
            ReservationBusiness bu = new ReservationBusiness();
            List<ReservationDetails> list = new List<ReservationDetails>();
            list = bu.GetReservationsWithNbArticles();

            return View(list);
        }
        [HttpPost]
        public IActionResult RemoveFromReservation(ReservationViewModel model)
        {
            ReservationBusiness bu = new ReservationBusiness();
            bu.EditDateAnnulation(model.IdSelectedDonToRemove);

            return RedirectToAction("DisplayReservation", new { idAdh = model.SelectedAdherent.Idadh });
        }
    }
}
