﻿using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class ProduitBusiness
    {
        public List<Produit> GetProduits()
        {
            ProduitDAO prodDAO = new ProduitDAO();
            return prodDAO.GetAll();
        }
        public List<Produit> GetProduitsById(int id)
        {
            ProduitDAO produitDAO = new ProduitDAO();
            return produitDAO.GetAllById(id);
        }
        public Produit GetProduitById(int id)
        {
            ProduitDAO produitDAO = new ProduitDAO();
            return produitDAO.GetById(id);
        }
    }
}
