﻿using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class DonBusiness
    {
        public void SaveDon(Don don)
        {
            DonDAO donDAO = new DonDAO();
            don.DateAjout = DateTime.Now;
            donDAO.Insert(don);
        }

        public List<DonDetails> GetAllDon()
        {
            DonDAO dao = new DonDAO();
            return dao.RechercheTotal();
        }

        public List<DonDetails> RechercheDonMultiCri(string catNom, string sousCatNom, string nomProduit, string villeNom)
        {
            DonDAO donDAO = new DonDAO();
            return donDAO.RechercheMultiCri(catNom, sousCatNom, nomProduit, villeNom);
        }
        public List<DonDetails> GetAllDonsFromIdAdh(int id)
        {
            DonDAO dao = new DonDAO();
            List<DonDetails> list = new List<DonDetails>();
            list = dao.GetAllByIdAdh(id);

            foreach (DonDetails d in list)
            {
                if (d.DateAjout.ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    d.Status = "Proposé le " + d.DateAjout.ToString("dd MMMM yyyy");
                    if (d.DateReservation.ToString("MM/dd/yyyy") != "01/01/0001")
                    {
                        d.Status = "Réservé le " + d.DateReservation.ToString("dd MMMM yyyy");
                        if (d.DateDelivery.ToString("MM/dd/yyyy") != "01/01/0001")
                        {
                            d.Status = "Livré le " + d.DateDelivery.ToString("dd MMMM yyyy");
                        }
                        else if (d.DateLimite <= DateTime.Now)
                        {
                            d.Status = "Périmé le " + d.DateLimite.ToString("dd MMMM yyyy");
                        }
                    }
                    else if (d.DateAnnulation.ToString("MM/dd/yyyy") != "01/01/0001")
                    {
                        d.Status = "Annulé le " + d.DateAnnulation.ToString("dd MMMM yyyy");
                    }
                    else if (d.DateLimite <= DateTime.Now)
                    {
                        d.Status = "Périmé le " + d.DateLimite.ToString("dd MMMM yyyy");
                    }
                }
            }

            return list;
        }

        public DonDetails ShowDetails(int id)
        {
            DonDAO dao = new DonDAO();
            return dao.GetByID(id);
        }
        public void CancelDon(int id, DateTime cancDate)
        {
            DonDAO dao = new DonDAO();
            dao.Cancel(id, cancDate);
        }
    }
}
