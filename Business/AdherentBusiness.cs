﻿using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class AdherentBusiness
    {
        public List<Adherent> GetAdherents()
        {
            AdherentDAO adhDAO = new AdherentDAO();
            return adhDAO.GetAll();
        }
        public Adherent GetAdherentbyId(int id)
        {
            AdherentDAO adhDAO = new AdherentDAO();
            return adhDAO.GetById(id);
        }
    }
}
