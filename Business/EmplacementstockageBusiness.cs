﻿using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class EmplacementstockageBusiness
    {
        public List<EmplacementstockageDetails> ListEmplacementDispoInfra(int Idinfra)
        {
            
            EmplacementstockageDAO empDAO = new EmplacementstockageDAO();
            return empDAO.ListEmplacementDispoInfra(Idinfra);
        }
        public List<EmplacementstockageDetails> ListEmplacementParPart(int Idpart)
        {
            
            EmplacementstockageDAO empDAO = new EmplacementstockageDAO();
            return empDAO.ListEmplacementParPart(Idpart);
        }
    }
}
