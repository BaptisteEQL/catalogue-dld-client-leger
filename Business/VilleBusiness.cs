﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class VilleBusiness
    {
        public List<Ville> GetVilles()
        {
            VilleDAO villeDAO = new VilleDAO();
            return villeDAO.GetAll();
        }

        public Ville RechercheVilleParId(int idville)
        {
            VilleDAO villeDAO=new VilleDAO();
            return villeDAO.RechercheVilleParId(idville);
        }

    }
}
