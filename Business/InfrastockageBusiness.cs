﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class InfrastockageBusiness
    {
        public List<InfrastockageDetails> ToutInfraDispoParVille(int idville)
        {
            InfrastockageDAO Infdao = new InfrastockageDAO();
            return Infdao.ToutInfraDispoParVille(idville);
        }
        public List<InfrastockageDetails> ToutInfraDispo()
        {
            InfrastockageDAO Infdao = new InfrastockageDAO();
            return Infdao.ToutInfraDispo();
        }
    }
}
