/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  13/12/2021 16:19:32                      */
/*==============================================================*/


/*==============================================================*/
/* Table : Achatcoupon                                          */
/*==============================================================*/
create table Achatcoupon
(
   idachat              int not null auto_increment,
   idadh                int not null,
   dateachatcoupon      datetime,
   primary key (idachat)
);

/*==============================================================*/
/* Table : Adherent                                             */
/*==============================================================*/
create table Adherent
(
   idadh                int not null auto_increment,
   idmotif              int,
   idville              int not null,
   numrue               int,
   complement           char(1),
   libelevoie           varchar(254),
   nom                  varchar(254),
   prenom               varchar(254),
   civilite             varchar(254),
   datenaissance        datetime,
   numtelephone         int,
   mail                 varchar(254),
   datevalidationinscription datetime,
   dateinscription      datetime,
   login                varchar(254),
   mdp                  varchar(254),
   datedesinscription   datetime,
   datedebutdesactivation datetime,
   datefindesactivation datetime,
   primary key (idadh)
);

/*==============================================================*/
/* Table : Categorie                                            */
/*==============================================================*/
create table Categorie
(
   idcategorie          int not null auto_increment,
   nom                  varchar(254),
   primary key (idcategorie)
);

alter table Categorie comment 'Exemple:
- Fruits et Legumes
- Produits laitiers';

/*==============================================================*/
/* Table : CompositionStockage                                  */
/*==============================================================*/
create table CompositionStockage
(
   idtypestockage       int not null,
   idinfrastockage      int not null,
   primary key (idtypestockage, idinfrastockage)
);

/*==============================================================*/
/* Table : Conditionconservation                                */
/*==============================================================*/
create table Conditionconservation
(
   idconditionconservation int not null auto_increment,
   nom                  varchar(254),
   primary key (idconditionconservation)
);

/*==============================================================*/
/* Table : Don                                                  */
/*==============================================================*/
create table Don
(
   iddon                int not null auto_increment,
   idadh                int not null,
   idunite              int not null,
   idproduit            int not null,
   idville              int,
   idemplacementstockage int,
   photo                varchar(254),
   quantite             int,
   datelimite           datetime,
   dateajout            datetime,
   daterecuperationdonannule datetime,
   daterecuperationdonperime datetime,
   dateannulation       datetime,
   datedepotstockage    datetime,
   numrue               int,
   libelevoie           varchar(254),
   complement           char(1),
   primary key (iddon)
);

/*==============================================================*/
/* Table : Emplacementstockage                                  */
/*==============================================================*/
create table Emplacementstockage
(
   idemplacementstockage int not null auto_increment,
   idinfrastockage      int not null,
   idtaille             int not null,
   numemplacement       int,
   primary key (idemplacementstockage)
);

/*==============================================================*/
/* Table : Forfait                                              */
/*==============================================================*/
create table Forfait
(
   idforfait            int not null auto_increment,
   nbcoupon             int,
   montant              int,
   primary key (idforfait)
);

/*==============================================================*/
/* Table : Infrastockage                                        */
/*==============================================================*/
create table Infrastockage
(
   idinfrastockage      int not null auto_increment,
   idpartenaire         int not null,
   idville              int,
   complementinfo       varchar(254),
   numrue               int,
   libelevoie           varchar(254),
   complement           char(1),
   primary key (idinfrastockage)
);

/*==============================================================*/
/* Table : JourSemaine                                          */
/*==============================================================*/
create table JourSemaine
(
   idJourS              int not null auto_increment,
   libelleJourS         varchar(254),
   primary key (idJourS)
);

/*==============================================================*/
/* Table : Mesurer                                              */
/*==============================================================*/
create table Mesurer
(
   idproduit            int not null,
   idunite              int not null,
   primary key (idproduit, idunite)
);

/*==============================================================*/
/* Table : Motifdesincription                                   */
/*==============================================================*/
create table Motifdesincription
(
   motif                varchar(254),
   idmotif              int not null auto_increment,
   primary key (idmotif)
);

/*==============================================================*/
/* Table : Motifreclamation                                     */
/*==============================================================*/
create table Motifreclamation
(
   idmotif              int not null auto_increment,
   nom                  varchar(254),
   primary key (idmotif)
);

/*==============================================================*/
/* Table : Partenaire                                           */
/*==============================================================*/
create table Partenaire
(
   idpartenaire         int not null auto_increment,
   idville              int not null,
   idmotif              int,
   nomsociete           varchar(254),
   numrue               int,
   complement           char(1),
   libelevoie           varchar(254),
   numtelephone         int,
   mail                 varchar(254),
   datevalidationinscription datetime,
   dateinscription      datetime,
   daterefus            datetime,
   login                varchar(254),
   mdp                  varchar(254),
   datedesinscription   datetime,
   datedebutdesactivation datetime,
   datefindesactivation datetime,
   primary key (idpartenaire)
);

/*==============================================================*/
/* Table : Periodeindisponibilite                               */
/*==============================================================*/
create table Periodeindisponibilite
(
   idadh                int not null,
   idpartenaire         int not null,
   datedebutindispo     datetime,
   datefinindispo       datetime,
   idIndispo            int,
   dateEnregistrement   Date,
   dateAnnulation       Date
);

/*==============================================================*/
/* Table : Plagehoraire                                         */
/*==============================================================*/
create table Plagehoraire
(
   heuredebut           datetime not null,
   heurefin             datetime,
   idplagehoraire       int not null auto_increment,
   idJourS              int,
   idinfrastockage      int not null,
   primary key (idplagehoraire)
);

/*==============================================================*/
/* Table : Produit                                              */
/*==============================================================*/
create table Produit
(
   idproduit            int not null auto_increment,
   idconditionconservation int not null,
   idsouscategorie      int not null,
   nomProduit           varchar(254),
   primary key (idproduit)
);

/*==============================================================*/
/* Table : Quantiteforfait                                      */
/*==============================================================*/
create table Quantiteforfait
(
   idforfait            int not null,
   idachat              int not null,
   quantite             int,
   primary key (idforfait, idachat)
);

/*==============================================================*/
/* Table : Reclamation                                          */
/*==============================================================*/
create table Reclamation
(
   idreclamation        int not null auto_increment,
   idmotif              int not null,
   idreservation        int not null,
   datecreation         datetime,
   datefermeture        datetime,
   message              varchar(254),
   primary key (idreclamation)
);

/*==============================================================*/
/* Table : Reservation                                          */
/*==============================================================*/
create table Reservation
(
   idreservation        int not null auto_increment,
   iddon                int not null,
   idadh                int not null,
   dateajoutpanier      datetime,
   datereservation      datetime,
   daterecuperation     datetime,
   dateannulation       datetime,
   primary key (idreservation)
);

/*==============================================================*/
/* Table : Souscategorie                                        */
/*==============================================================*/
create table Souscategorie
(
   idsouscategorie      int not null auto_increment,
   idcategorie          int not null,
   nom                  varchar(254),
   primary key (idsouscategorie)
);

alter table Souscategorie comment 'Exemple:
- (Fruits et Legumes -> ) Legumes
- (Fr';

/*==============================================================*/
/* Table : Tailleemplacement                                    */
/*==============================================================*/
create table Tailleemplacement
(
   idtaille             int not null auto_increment,
   nom                  varchar(254),
   dimension            varchar(254),
   primary key (idtaille)
);

/*==============================================================*/
/* Table : Typestockage                                         */
/*==============================================================*/
create table Typestockage
(
   idtypestockage       int not null auto_increment,
   idconditionconservation int not null,
   nom                  varchar(254),
   primary key (idtypestockage)
);

/*==============================================================*/
/* Table : Unite                                                */
/*==============================================================*/
create table Unite
(
   idunite              int not null auto_increment,
   nom                  varchar(254),
   primary key (idunite)
);

/*==============================================================*/
/* Table : Ville                                                */
/*==============================================================*/
create table Ville
(
   idville              int not null auto_increment,
   codepostal           int,
   nom                  varchar(254),
   primary key (idville)
);

alter table Achatcoupon add constraint FK_Association3 foreign key (idadh)
      references Adherent (idadh) on delete restrict on update restrict;

alter table Adherent add constraint FK_Association1 foreign key (idville)
      references Ville (idville) on delete restrict on update restrict;

alter table Adherent add constraint FK_Association25 foreign key (idmotif)
      references Motifdesincription (idmotif) on delete restrict on update restrict;

alter table CompositionStockage add constraint FK_CompositionStockageInfra foreign key (idinfrastockage)
      references Infrastockage (idinfrastockage) on delete restrict on update restrict;

alter table CompositionStockage add constraint FK_CompositionStockageType foreign key (idtypestockage)
      references Typestockage (idtypestockage) on delete restrict on update restrict;

alter table Don add constraint FK_Association19 foreign key (idproduit)
      references Produit (idproduit) on delete restrict on update restrict;

alter table Don add constraint FK_Association22 foreign key (idemplacementstockage)
      references Emplacementstockage (idemplacementstockage) on delete restrict on update restrict;

alter table Don add constraint FK_Association28 foreign key (idunite)
      references Unite (idunite) on delete restrict on update restrict;

alter table Don add constraint FK_Association38 foreign key (idville)
      references Ville (idville) on delete restrict on update restrict;

alter table Don add constraint FK_Association6 foreign key (idadh)
      references Adherent (idadh) on delete restrict on update restrict;

alter table Emplacementstockage add constraint FK_Association26 foreign key (idinfrastockage)
      references Infrastockage (idinfrastockage) on delete restrict on update restrict;

alter table Emplacementstockage add constraint FK_Association31 foreign key (idtaille)
      references Tailleemplacement (idtaille) on delete restrict on update restrict;

alter table Infrastockage add constraint FK_Association27 foreign key (idpartenaire)
      references Partenaire (idpartenaire) on delete restrict on update restrict;

alter table Infrastockage add constraint FK_Association37 foreign key (idville)
      references Ville (idville) on delete restrict on update restrict;

alter table Mesurer add constraint FK_Mesurer_produit foreign key (idproduit)
      references Produit (idproduit) on delete restrict on update restrict;

alter table Mesurer add constraint FK_Mesurer_unite foreign key (idunite)
      references Unite (idunite) on delete restrict on update restrict;

alter table Partenaire add constraint FK_Association21 foreign key (idville)
      references Ville (idville) on delete restrict on update restrict;

alter table Partenaire add constraint FK_Association24 foreign key (idmotif)
      references Motifdesincription (idmotif) on delete restrict on update restrict;

alter table Periodeindisponibilite add constraint FK_Association35 foreign key (idpartenaire)
      references Partenaire (idpartenaire) on delete restrict on update restrict;

alter table Periodeindisponibilite add constraint FK_Association36 foreign key (idadh)
      references Adherent (idadh) on delete restrict on update restrict;

alter table Plagehoraire add constraint FK_Association13 foreign key (idinfrastockage)
      references Infrastockage (idinfrastockage) on delete restrict on update restrict;

alter table Plagehoraire add constraint FK_Association33 foreign key (idJourS)
      references JourSemaine (idJourS) on delete restrict on update restrict;

alter table Produit add constraint FK_Association30 foreign key (idconditionconservation)
      references Conditionconservation (idconditionconservation) on delete restrict on update restrict;

alter table Produit add constraint FK_Association40 foreign key (idsouscategorie)
      references Souscategorie (idsouscategorie) on delete restrict on update restrict;

alter table Quantiteforfait add constraint FK_contient foreign key (idforfait)
      references Forfait (idforfait) on delete restrict on update restrict;

alter table Quantiteforfait add constraint FK_est_contenu_par foreign key (idachat)
      references Achatcoupon (idachat) on delete restrict on update restrict;

alter table Reclamation add constraint FK_Association17 foreign key (idreservation)
      references Reservation (idreservation) on delete restrict on update restrict;

alter table Reclamation add constraint FK_Association41 foreign key (idmotif)
      references Motifreclamation (idmotif) on delete restrict on update restrict;

alter table Reservation add constraint FK_Association14 foreign key (iddon)
      references Don (iddon) on delete restrict on update restrict;

alter table Reservation add constraint FK_Association16 foreign key (idadh)
      references Adherent (idadh) on delete restrict on update restrict;

alter table Souscategorie add constraint FK_Association39 foreign key (idcategorie)
      references Categorie (idcategorie) on delete restrict on update restrict;

alter table Typestockage add constraint FK_Association32 foreign key (idconditionconservation)
      references Conditionconservation (idconditionconservation) on delete restrict on update restrict;

