﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Ville
    {
        public int Idville { get; set; }
        public int Codepostal { get; set; }
        public string Nom { get; set; }

        public Ville(int idville, int codepostal, string nom)
        {
            Idville = idville;
            Codepostal = codepostal;
            Nom = nom;
        }

        public Ville()
        {
        }
    }
}
