﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Reservation
    {
        public int IdReservation { get; set; }
        [Required(ErrorMessage = "L'ID de la réservation est obligatoire")]
        public int IdDon { get; set; }  /*fk Don*/
        [Required(ErrorMessage = "L'ID du don est obligatoire")]
        public int IdAdh { get; set; }  /*fk Adherent*/
        [Required(ErrorMessage = "L'ID de l'adhérent est obligatoire")]
        public DateTime DateAjoutPanier { get; set; }
        public DateTime DateReservation { get; set; }
        public DateTime DateRecuperation { get; set; }
        public DateTime DateAnnulation { get; set; }
    }
}
