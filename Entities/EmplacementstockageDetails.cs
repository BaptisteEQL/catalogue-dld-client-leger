﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class EmplacementstockageDetails:Emplacementstockage
    {
        public int NumrueInf { get; set; }
        public string LibelevoieInf { get; set; }
        public char? ComplementInf { get; set; }
        public int NumruePart { get; set; }
        public string LibelevoiePart { get; set; }
        public char? ComplementPart { get; set; }
        public int Codepostal { get; set; }
        public string Nomville { get; set; }
        public string Dimension { get; set; }
        public int? Iddonemplacement { get; set; }
        public int? Idville { get; set; }
        public EmplacementstockageDetails()
        {
        }

        public EmplacementstockageDetails(int numrueInf, string libelevoieInf, char? complementInf, 
            int numruePart, string libelevoiePart, char? complementPart, int codepostal, string nomville, 
            string dimension, int? iddonemplacement, int? idville)
        {
            NumrueInf = numrueInf;
            LibelevoieInf = libelevoieInf;
            ComplementInf = complementInf;
            NumruePart = numruePart;
            LibelevoiePart = libelevoiePart;
            ComplementPart = complementPart;
            Codepostal = codepostal;
            Nomville = nomville;
            Dimension = dimension;
            Iddonemplacement = iddonemplacement;
            Idville = idville;
        }
    }
}
