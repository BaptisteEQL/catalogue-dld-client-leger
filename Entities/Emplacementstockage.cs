﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Emplacementstockage
    {
        public int Idemplacementstockage { get; set; }
        public int Idinfrastockage { get; set; }
        public int Idtaille { get; set; }
        public int Numemplacement { get; set; }

    }
}
