﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Souscategorie
    {
        public int IdSousCategorie { get; set; }
        public int IdCategorie { get; set; } /*fk Categorie*/
        public string Nom { get; set; }

        public Souscategorie(int idSousCategorie, int idCategorie, string nom)
        {
            IdSousCategorie = idSousCategorie;
            IdCategorie = idCategorie;
            Nom = nom;
        }
        public Souscategorie()
        {
        }
    }
}
