﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Typestockage
    {
        public int Idtypestockage { get; set; }
        public int Idconditionconservation { get; set; }
        public string Nom { get; set; }
    }
}
