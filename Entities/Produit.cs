﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Produit
    {
        public int IdProduit { get; set; }
        public int IdConditionConservation { get; set; } /*fk Unite*/
        public int IdSousCategorie { get; set; } /*fk Unite*/
        public string NomProduit { get; set; }

        public Produit(int idProduit, int idConditionConservation, int idSousCategorie, string nomProduit)
        {
            IdProduit = idProduit;
            IdConditionConservation = idConditionConservation;
            IdSousCategorie = idSousCategorie;
            NomProduit = nomProduit;
        }

        public Produit()
        {
        }
    }
}
