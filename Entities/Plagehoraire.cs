﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Plagehoraire
    {
        public DateTime Heuredebut { get; set; }
        public DateTime Heurefin { get; set; }
        public int Idplagehoraire { get; set; }
        public int IdJourS { get; set; }
        public int Idinfrastockage { get; set; }
    }
}
