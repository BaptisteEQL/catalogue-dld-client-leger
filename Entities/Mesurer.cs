﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Mesurer
    {
        public int IdProduit { get; set; }  /*fk Produit*/
        public int IdUnite { get; set; }  /*fk Unite*/
    }
}
