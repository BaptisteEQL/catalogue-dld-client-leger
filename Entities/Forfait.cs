﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Forfait
    {
        public int Idforfait { get; set; }
        public int Nbcoupon { get; set; }
        public int Montant { get; set; }
    }
}
