﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class ReservationDetails : Reservation
    {
        public string NameProduit { get; set; }
        public int NbQuantity { get; set; }
        public string NameUnity { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int NbStreet { get; set; }
        public char ComplementStreet { get; set; }
        public string NameStreet { get; set; }
        public int PostalCode { get; set; }
        public string NameCity { get; set; }
        public string NameAdherent { get; set; }
        public string FirstNameAdherent { get; set; }
        public int NbEmplacement { get; set; }
        public int NbArticles { get; set; }
        public string NameSociety { get; set; }
    }
}
